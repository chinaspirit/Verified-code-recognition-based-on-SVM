﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * 作者：王宾鲁
 * 本类用于SVM算法的训练样本和各参数的整合
 */

namespace SVM
{
    class OptStruct
    {
        public Matrix X;
        public Matrix labelMat;
        public double C;
        public double tol;
        public int m;
        public Matrix alphas;
        public double b;
        public Matrix eCache;
        public Matrix K;

        /// <summary>
        /// 用于整合训练集，标记及其他参数的类
        /// </summary>
        /// <param name="dataMatIn">训练样本集</param>
        /// <param name="classLabels">训练样本标记</param>
        /// <param name="C">惩罚系数</param>
        /// <param name="toler">松弛变量</param>
        /// <param name="kT">核函数类型</param>
        /// <param name="sigma">高斯核函数参数值</param>
        public OptStruct(Matrix dataMatIn, Matrix classLabels, double C, double toler, kernelType kT, double sigma=1)
        {
            X = dataMatIn;
            labelMat = classLabels;
            this.C = C;
            tol = toler;
            m = dataMatIn.Row;
            alphas = new Matrix(m, 1);
            b = 0;
            eCache = new Matrix(m, 2);
            K = new Matrix(m, m);
            for(int i=0;i< m; i++)
            {
                Matrix Xi = new Matrix(1, X.Col);
                for(int t = 0; t < X.Col; t++)
                {
                    Xi[0, t] = X[i, t];
                }
                Matrix XTrans = kernelTrans(X, Xi, kT, sigma);
                for (int j=0;j< m; j++)
                {
                    K[j, i] = XTrans[j, 0];
                }
            }
        }

        /// <summary>
        /// 用于核函数变换
        /// </summary>
        /// <param name="X">支持向量</param>
        /// <param name="A">待转换参数</param>
        /// <param name="kT">核函数类型</param>
        /// <param name="sigma">高斯核函数参数值</param>
        /// <returns></returns>
        public static Matrix kernelTrans(Matrix X, Matrix A, kernelType kT, double sigma)
        {
            int row = X.Row;
            int column = X.Col;
            Matrix K = new Matrix(row, 1);
            if (kT == kernelType.lin)
            {
                K = X * A.Transpose();
            }
            else if (kT == kernelType.rbf)
            {
                Matrix deltaRow = new Matrix(1, X.Col);
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < column; j++)
                    {
                        deltaRow[0, j] = X[i, j] - A[0, j];
                    }
                    K[i, 0] = (deltaRow * deltaRow.Transpose()).ToDouble();
                    K[i, 0] = Math.Pow(Math.E, K[i, 0] / ((-1) * sigma * sigma));
                }
            }
            return K;
        }
    }
}
