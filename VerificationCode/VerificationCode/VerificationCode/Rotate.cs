﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
 * 作者：文晴曼
 * 本类：将图片以矩阵形式旋转到一定角度
 */
namespace SVM
{
    class Rotate
    {
		/// <summary>
		/// 选择旋转
		/// </summary>
		/// <returns>旋转后矩阵</returns>
		/// <param name="inMatrix">原矩阵</param>
		/// <param name="comparePositive">比较次数.</param>
		/// <param name="rad">旋转弧度</param>
        public static Matrix chooseRotate(Matrix inMatrix, int comparePositive, double rad)
        {
            int row = inMatrix.Row;	//获取输入矩阵的宽度
            int col = inMatrix.Col;	//获取输入矩阵的高度
            int[,] columnTotal = new int[comparePositive * 2 + 1, col];
            int[,] rowTotal = new int[comparePositive * 2 + 1, row];
            int rowmax = row;
            int colmin = col;
            int rowlong = 0;
            int collong = 0;
            Matrix outMatrix = new Matrix(row, col);
            Matrix rotateM = new Matrix(row, col);
            for (int i = -comparePositive; i < comparePositive + 1; i++)
            {
                if (i == 0)
                {
                    rotateM = inMatrix;
				}	//当比较数量comparePositive等于0，不旋转
                else
                {
                    rotateM = rotateMatrix(inMatrix, rad * i);
				}	//当比较数量comparePositive不等于0，旋转，调用rotateMatrix方法
                for (int m = 0; m < row; m++)
                {
                    for (int j = 0; j < col; j++)
                    {
                        if (rotateM[m, j] > 0)
                        {
                            rowlong++;
                            break;
                        }
                    }
                }	//测量字母所占的行数
                for (int m = 0; m < col; m++)
                {
                    for (int j = 0; j < row; j++)
                    {
                        if (rotateM[j, m] > 0)
                        {
                            collong++;
                            break;
                        }
                    }
                }	//测量字母所占的列数
                //Console.WriteLine("{0},{1}", collong, rowlong);
                if (rowlong * collong < colmin * rowmax)
                {
                    rowmax = rowlong;
                    colmin = collong;
                    outMatrix = rotateM;
                }
                //Console.WriteLine("{0}-{1}", colmin, rowmax);
                rowlong = collong = 0;
            }
            return outMatrix;
        }
		/// <summary>
		/// 旋转过程
		/// </summary>
		/// <returns>旋转后的矩阵</returns>
		/// <param name="inMatrix">原矩阵</param>
		/// <param name="theta">旋转角度</param>
        public static Matrix rotateMatrix(Matrix inMatrix, double theta)
        {
            int row = inMatrix.Row;
            int col = inMatrix.Col;
            int mrow = (row - 1) / 2;	//找到行数中点
            int mcol = (col - 1) / 2;	//找到列数中点
            int x, y, xnew, ynew, rownew, colnew;
            Matrix outMatrix = new Matrix(row, col);	//旋转后的矩阵和原矩阵具有相同的行数和列数
            outMatrix.SetValue(0);	//初始化，先将新矩阵的所有点的值都设置为0
     
            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < row; j++)
                {
                    if (inMatrix[i, j] == 1)
                    {
                        x = i - mcol;
                        y = j - mrow;
                        ynew = (int)(x * Math.Cos(theta) - y * Math.Sin(theta)) ;
                        xnew = (int)(x * Math.Sin(theta) + y * Math.Cos(theta)) ;
                        rownew = ynew + mrow;
                        colnew = xnew + mcol;
                        if ((rownew < 0) || (rownew > row - 1) || (colnew < 0) || (colnew > col - 1))
                        {
                            continue;
                        }
                        else
                        {
                            outMatrix[rownew, colnew] = 1;	//将旋转后与原矩阵对应的位置的值设为1
                        }
                    }
                }
            }
            
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (i == 0 || j == 0 || i == row - 1 || j == col - 1) continue;
                    if (outMatrix[i, j] == 0)
                    {
                        double total = 0;
                        total += outMatrix[i - 1, j] + outMatrix[i + 1, j];
                        total += outMatrix[i, j - 1] + outMatrix[i, j + 1];
                        if (total > 2)
                        {
							outMatrix[i, j] = 1;	//将旋转后与原矩阵对应的位置的值设为1
                        }
                    }
                }
            }            
            return outMatrix;
        }
    }
}
