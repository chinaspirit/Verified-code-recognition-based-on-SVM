﻿using System.Drawing;
namespace SVM
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PicName = new System.Windows.Forms.Label();
            this.OpenPic = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ShowLetters = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SVMButton = new System.Windows.Forms.RadioButton();
            this.knnButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(12, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 93);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // PicName
            // 
            this.PicName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PicName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PicName.Location = new System.Drawing.Point(12, 109);
            this.PicName.Name = "PicName";
            this.PicName.Size = new System.Drawing.Size(256, 35);
            this.PicName.TabIndex = 1;
            this.PicName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // OpenPic
            // 
            this.OpenPic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.OpenPic.Location = new System.Drawing.Point(6, 10);
            this.OpenPic.Name = "OpenPic";
            this.OpenPic.Size = new System.Drawing.Size(120, 26);
            this.OpenPic.TabIndex = 2;
            this.OpenPic.Text = "打开图片";
            this.OpenPic.UseVisualStyleBackColor = true;
            this.OpenPic.Click += new System.EventHandler(this.OpenPic_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.ShowLetters);
            this.groupBox1.Controls.Add(this.OpenPic);
            this.groupBox1.Location = new System.Drawing.Point(12, 200);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(257, 42);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // ShowLetters
            // 
            this.ShowLetters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ShowLetters.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ShowLetters.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ShowLetters.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ShowLetters.Location = new System.Drawing.Point(131, 10);
            this.ShowLetters.Name = "ShowLetters";
            this.ShowLetters.Size = new System.Drawing.Size(120, 26);
            this.ShowLetters.TabIndex = 3;
            this.ShowLetters.Text = "显示图中字母";
            this.ShowLetters.UseVisualStyleBackColor = true;
            this.ShowLetters.Click += new System.EventHandler(this.ShowPicName_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.SVMButton);
            this.groupBox2.Controls.Add(this.knnButton);
            this.groupBox2.Location = new System.Drawing.Point(12, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 34);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "选择算法";
            // 
            // SVMButton
            // 
            this.SVMButton.AutoSize = true;
            this.SVMButton.Location = new System.Drawing.Point(185, 12);
            this.SVMButton.Name = "SVMButton";
            this.SVMButton.Size = new System.Drawing.Size(41, 16);
            this.SVMButton.TabIndex = 1;
            this.SVMButton.TabStop = true;
            this.SVMButton.Text = "SVM";
            this.SVMButton.UseVisualStyleBackColor = true;
            // 
            // knnButton
            // 
            this.knnButton.AutoSize = true;
            this.knnButton.Location = new System.Drawing.Point(111, 12);
            this.knnButton.Name = "knnButton";
            this.knnButton.Size = new System.Drawing.Size(41, 16);
            this.knnButton.TabIndex = 0;
            this.knnButton.TabStop = true;
            this.knnButton.Text = "knn";
            this.knnButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 259);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.PicName);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "验证码识别";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label PicName;
        private System.Windows.Forms.Button OpenPic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button ShowLetters;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton SVMButton;
        private System.Windows.Forms.RadioButton knnButton;

    }
}

