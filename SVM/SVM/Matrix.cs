﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
 * 作者：李湘，刘祥霖
 * 本类为矩阵运算的实现
 */


namespace SVM
{
    public class Matrix
    {
        /// <summary>
        /// 生成方阵
        /// </summary>
        /// <param name="row">矩阵维数</param>
        public Matrix(int row)
        {
            m_data = new double[row, row];
        }
        /// <summary>
        /// 生成矩阵
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public Matrix(int row, int col)
        {
            m_data = new double[row, col];
        }
        /// <summary>
        /// 复制构造函数
        /// </summary>
        /// <param name="m">待复制矩阵</param>
        public Matrix(Matrix m)
        {
            int row = m.Row;
            int col = m.Col;
            m_data = new double[row, col];

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                    m_data[i, j] = m[i, j];
        }

        /// <summary>
        /// 将矩阵设置为单位矩阵
        /// </summary>
        public void SetUnit()
        {
            for (int i = 0; i < m_data.GetLength(0); i++)
                for (int j = 0; j < m_data.GetLength(1); j++)
                    m_data[i, j] = ((i == j) ? 1 : 0);
        }

        /// <summary>
        /// 将矩阵全部设为某个值
        /// </summary>
        /// <param name="d"></param>
        public void SetValue(double d)
        {
            for (int i = 0; i < m_data.GetLength(0); i++)
                for (int j = 0; j < m_data.GetLength(1); j++)
                    m_data[i, j] = d;
        }

        /// <summary>
        /// 矩阵标记
        /// </summary>
        private int flag;
        public int Flag
        {
            get
            {
                return flag;
            }
            set
            {
                flag = value;
            }
        }


        /// <summary>
        /// 返回矩阵行数
        /// </summary>
        public int Row
        {
            get
            {
                return m_data.GetLength(0);
            }
        }

        /// <summary>
        /// 返回矩阵列数
        /// </summary>
        public int Col
        {
            get
            {
                return m_data.GetLength(1);
            }
        }

        /// <summary>
        /// 索引器重载，获取矩阵元素
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns>矩阵元素值</returns>
        public double this[int row, int col]
        {
            get
            {
                //if ((row < 0) || (row > Row - 1) || (col < 0) || (col > Col - 1)) return 0;
                return m_data[row, col];
            }
            set
            {
                //if ((row < 0) || (row > Row - 1) || (col < 0) || (col > Col - 1)) return;
                m_data[row, col] = value;
            }
        }

        /// <summary>
        /// 获取矩阵的某一行
        /// </summary>
        /// <param name="row"></param>
        /// <returns>由该行生成的行矩阵</returns>
        public Matrix getRow(int row)
        {
            Matrix mat = new Matrix(1, Col);
            for(int i = 0; i < Col; i++)
            {
                mat[0, i] = this[row, i];
            }
            return mat;
        }

        /// <summary>
        /// 获取矩阵的某一列
        /// </summary>
        /// <param name="col"></param>
        /// <returns>由该列生成的列矩阵</returns>
        public Matrix getCol(int col)
        {
            Matrix mat = new Matrix(Row, 1);
            for (int i = 0; i < Row; i++)
            {
                mat[i, 0] = this[i, col];
            }
            return mat;
        }


        /// <summary>
        /// 交换矩阵的某两行
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns>交换后的矩阵</returns>
        public Matrix Exchange(int i, int j)
        {
            double temp;

            for (int k = 0; k < Col; k++)
            {
                temp = m_data[i, k];
                m_data[i, k] = m_data[j, k];
                m_data[j, k] = temp;
            }
            return this;
        }


        /// <summary>
        /// 在第index行乘上系数mul
        /// </summary>
        /// <param name="index"></param>
        /// <param name="mul"></param>
        /// <returns></returns>
        Matrix Multiple(int index, double mul)
        {
            for (int j = 0; j < Col; j++)
            {
                m_data[index, j] *= mul;
            }
            return this;
        }


        /// <summary>
        /// 第src行乘以mul加到第index行
        /// </summary>
        /// <param name="index"></param>
        /// <param name="src"></param>
        /// <param name="mul"></param>
        /// <returns></returns>
        Matrix MultipleAdd(int index, int src, double mul)
        {
            for (int j = 0; j < Col; j++)
            {
                m_data[index, j] += m_data[src, j] * mul;
            }

            return this;
        }

        /// <summary>
        /// 矩阵转制
        /// </summary>
        /// <returns></returns>
        public Matrix Transpose()
        {
            Matrix ret = new Matrix(Col, Row);

            for (int i = 0; i < Row; i++)
                for (int j = 0; j < Col; j++)
                {
                    ret[j, i] = m_data[i, j];
                }
            return ret;
        }

        /// <summary>
        /// 矩阵相加
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns>相加后的新矩阵</returns>
        public static Matrix operator +(Matrix lhs, Matrix rhs)
        {
            if (lhs.Row != rhs.Row)    //异常
            {
                System.Exception e = new Exception("相加的两个矩阵的行数不等");
                throw e;
            }
            if (lhs.Col != rhs.Col)     //异常
            {
                System.Exception e = new Exception("相加的两个矩阵的列数不等");
                throw e;
            }

            int row = lhs.Row;
            int col = lhs.Col;
            Matrix ret = new Matrix(row, col);

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                {
                    double d = lhs[i, j] + rhs[i, j];
                    ret[i, j] = d;
                }
            return ret;

        }

        /// <summary>
        /// 矩阵相减
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Matrix operator -(Matrix lhs, Matrix rhs)
        {
            if (lhs.Row != rhs.Row)    //异常
            {
                System.Exception e = new Exception("相减的两个矩阵的行数不等");
                throw e;
            }
            if (lhs.Col != rhs.Col)     //异常
            {
                System.Exception e = new Exception("相减的两个矩阵的列数不等");
                throw e;
            }

            int row = lhs.Row;
            int col = lhs.Col;
            Matrix ret = new Matrix(row, col);

            for (int i = 0; i < row; i++)
                for (int j = 0; j < col; j++)
                {
                    double d = lhs[i, j] - rhs[i, j];
                    ret[i, j] = d;
                }
            return ret;
        }


        /// <summary>
        /// 矩阵相乘
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns>相乘后的矩阵</returns>
        public static Matrix operator *(Matrix lhs, Matrix rhs)
        {
            if (lhs.Col != rhs.Row)    //异常
            {
                System.Exception e = new Exception("相乘的两个矩阵的行列数不匹配");
                throw e;
            }
            Matrix ret = new Matrix(lhs.Row, rhs.Col);
            double temp;
            for (int i = 0; i < lhs.Row; i++)
            {
                for (int j = 0; j < rhs.Col; j++)
                {
                    temp = 0;
                    for (int k = 0; k < lhs.Col; k++)
                    {
                        temp += lhs[i, k] * rhs[k, j];
                    }
                    ret[i, j] = temp;
                }
            }

            return ret;
        }


        /// <summary>
        /// 矩阵相除
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Matrix operator /(Matrix lhs, Matrix rhs)
        {
            return lhs * rhs.Inverse();
        }

        /// <summary>
        /// 矩阵元素相加
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix operator +(Matrix m)
        {
            Matrix ret = new Matrix(m);
            return ret;
        }

        /// <summary>
        /// 矩阵元素相减
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix operator -(Matrix m)
        {
            Matrix ret = new Matrix(m);
            for (int i = 0; i < ret.Row; i++)
                for (int j = 0; j < ret.Col; j++)
                {
                    ret[i, j] = -ret[i, j];
                }

            return ret;
        }

        /// <summary>
        /// 矩阵元素相乘
        /// </summary>
        /// <param name="d"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix operator *(double d, Matrix m)
        {
            Matrix ret = new Matrix(m);
            for (int i = 0; i < ret.Row; i++)
                for (int j = 0; j < ret.Col; j++)
                    ret[i, j] *= d;

            return ret;
        }

        /// <summary>
        /// 数除
        /// </summary>
        /// <param name="d"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Matrix operator /(double d, Matrix m)
        {
            return d * m.Inverse();
        }

        /// <summary>
        /// 矩阵元素数除
        /// </summary>
        /// <param name="m"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Matrix operator /(Matrix m, double d)
        {
            Matrix ret = new Matrix(m);
            for (int i = 0; i < ret.Row; i++)
                for (int j = 0; j < ret.Col; j++)
                    ret[i, j] /= d;
            return ret;
        }
        /// <summary>
        /// 返回列主元素的行号
        /// </summary>
        /// <param name="row">开始查找的行号</param>
        /// <returns>在行号[row,Col)范围内查找第row列中绝对值最大的元素，返回所在行号</returns>
        int Pivot(int row)
        {
            int index = row;

            for (int i = row + 1; i < Row; i++)
            {
                if (m_data[i, row] > m_data[index, row])
                    index = i;
            }

            return index;
        }

        /// <summary>
        /// 求矩阵逆矩阵
        /// </summary>
        /// <returns></returns>
        public Matrix Inverse()
        {
            if (Row != Col)    //异常,非方阵
            {
                System.Exception e = new Exception("求逆的矩阵不是方阵");
                throw e;
            }
            Matrix tmp = new Matrix(this);
            Matrix ret = new Matrix(Row);    //单位阵
            ret.SetUnit();

            int maxIndex;
            double dMul;

            for (int i = 0; i < Row; i++)
            {
                maxIndex = tmp.Pivot(i);

                if (tmp.m_data[maxIndex, i] == 0)
                {
                    System.Exception e = new Exception("求逆的矩阵的行列式的值等于0,");
                    throw e;
                }

                if (maxIndex != i)    //下三角阵中此列的最大值不在当前行，交换
                {
                    tmp.Exchange(i, maxIndex);
                    ret.Exchange(i, maxIndex);
                }

                ret.Multiple(i, 1 / tmp[i, i]);
                tmp.Multiple(i, 1 / tmp[i, i]);

                for (int j = i + 1; j < Row; j++)
                {
                    dMul = -tmp[j, i] / tmp[i, i];
                    tmp.MultipleAdd(j, i, dMul);

                }
            }
            for (int i = Row - 1; i > 0; i--)
            {
                for (int j = i - 1; j >= 0; j--)
                {
                    dMul = -tmp[j, i] / tmp[i, i];
                    tmp.MultipleAdd(j, i, dMul);
                    ret.MultipleAdd(j, i, dMul);
                }
            }
            return ret;
        }

        /// <summary>
        /// 判断是否为方阵
        /// </summary>
        /// <returns></returns>
        public bool IsSquare()
        {
            return Row == Col;
        }

        /// <summary>
        /// 判断是否为对称矩阵
        /// </summary>
        /// <returns></returns>
        public bool IsSymmetric()
        {

            if (Row != Col)
                return false;

            for (int i = 0; i < Row; i++)
                for (int j = i + 1; j < Col; j++)
                    if (m_data[i, j] != m_data[j, i])
                        return false;

            return true;
        }

        /// <summary>
        /// 将一阶方阵转为double
        /// </summary>
        /// <returns>一阶方阵中唯一的元素</returns>
        public double ToDouble()
        {
            Trace.Assert(Row == 1 && Col == 1);
            return m_data[0, 0];
        }

        /// <summary>
        /// 将矩阵转为字符串输出
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                    s += string.Format("{0} ", m_data[i, j]);

                s += "\n";
            }
            return s;
        }

        /// <summary>
        /// 矩阵元素相乘
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Matrix Multiply(Matrix A, Matrix B)
        {
            if (A.Row != B.Row || A.Col != B.Col)
            {
                System.Exception e = new Exception("矩阵行列不匹配");
                throw e;
            }
            Matrix C = new Matrix(A.Row, A.Col); 
            for(int i = 0; i < C.Row; i++)
            {
                for(int j = 0; j < C.Col; j++)
                {
                    C[i, j] = A[i, j] * B[i, j];
                }
            }
            return C;
        }

        ///私有数据成员
        private double[,] m_data;
    }
}
