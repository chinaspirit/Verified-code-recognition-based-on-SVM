using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

public enum kernelType { lin, rbf };

namespace SVM
{
    
    class MainA
    {
        static void Main(string[] args)
        {
          
            //通过读文件创建矩阵


                //Matrix m = new Matrix(30, 120);
                //FileStream fs = new FileStream("Matrix10.txt", FileMode.Open);
                //StreamReader reader = new StreamReader(fs, Encoding.Default);
                //for (int i = 0; i < m.Row; i++)
                //{
                //    string text = reader.ReadLine();
                //    Console.WriteLine(text);
                //    for (int j = 0; j < m.Col; j++)
                //    {
                //        m[i, j] = int.Parse(text.ElementAt(j).ToString());
                //    }
                //}



                // 读取分类函数
                //Matrix m = new Matrix(30, 120);
                // FileStream fs = new FileStream("wandb/ws.txt", FileMode.Open);
                // StreamReader reader = new StreamReader(fs, Encoding.Default);
                // Matrix data = new Matrix(666, 625);
                // Matrix label = new Matrix(500, 1);
                // for (int i = 0; i < data.Row; i++)
                // {
                //     string text = reader.ReadLine();
                //     string[] texts = text.Split(' ');
                //     for (int j = 0; j < data.Col; j++)
                //     {
                //         data[i, j] = double.Parse(texts[j]);
                //     }
                // }
                // Console.WriteLine("finish{0},{1},{2},{3}", data[211, 59], data[624, 78], data[102, 600], data[12, 600]);


                //double b = 0;
                //int[] ktup = new int[2];
                //ktup[0] = 0;
                //ktup[1] = 0;
                //Matrix a = smoP(data, label, 0.6, 0.001, 100, ktup, ref b);
                //Matrix w = calcWs(a, data, label);
                //Console.WriteLine("{0},{1}___{2}", w[0, 0], w[1, 0], b);
                //for (int i = 0; i < 100; i++)
                //{
                //    Console.WriteLine((data[i, 0] * w[0, 0] + data[i, 1] * w[1, 0] + b) * label[i, 0] > 0);
                //}
                ////Console.WriteLine(label.ToString());
                //Console.WriteLine((7 * w[0, 0] + 2 * w[1, 0] + b));


                //Console.WriteLine(data.Row + "---------");


                //FileStream fs = new FileStream("testSetRBF.txt", FileMode.Open);
                //StreamReader reader = new StreamReader(fs, Encoding.Default);
                //Matrix data = new Matrix(100, 2);
                //Matrix label = new Matrix(100, 1);
                //for (int i = 0; i < data.Row; i++)
                //{
                //    string text = reader.ReadLine();
                //    data[i, 0] = double.Parse(text.Split('\t').ElementAt(0).ToString());
                //    data[i, 1] = double.Parse(text.Split('\t').ElementAt(1).ToString());
                //    label[i, 0] = double.Parse(text.Split('\t').ElementAt(2).ToString());
                //}
                //double b = 0;
                //Matrix a = SVM.smoP(data, label, 200, 0.0001, 40, kernelType.rbf, 1.3, ref b);
                //int svms = 0;
                //for(int i = 0; i < data.Row; i++)
                //{
                //    if (a[i, 0] > 0)
                //    {
                //        svms++;
                //    }
                //}
                //Console.WriteLine("svm number:{0}", svms);
                //Matrix w = SVM.calcWs(a, data, label);
                //Console.WriteLine("{0},{1}___{2}", w[0, 0], w[1, 0], b);
                //for (int i = 0; i < 100; i++)
                //{
                //    Console.WriteLine(((OptStruct.kernelTrans(data, data.getRow(i), kernelType.rbf, 1.3).Transpose() * Matrix.Multiply(a, label))[0, 0] + b) * label[i, 0] > 0);
                //}
                ////Console.WriteLine(a.ToString());
                //Console.WriteLine((7 * w[0, 0] + 2 * w[1, 0] + b));
                //Console.ReadKey();
                //SVM.testTwoDigits(1, 0.001, kernelType.lin, 10, 6, 11);
                //SVM.produceWsBsAs(1, 0.001, kernelType.lin, 10);
                //Console.WriteLine(IdentifyCode.identifyLetter("pic/81.jpg",kernelType.rbf));

                //一对多训练

            //暂时注释掉
            //    List<Matrix> allMatrix = new List<Matrix>();
            //int[] rowNum = new int[36];
            //for(int i = 0; i < 36; i++)
            //{
            //    string name = string.Format("/train/{0}", i);
            //    Matrix newMat = LetterMatrixForTrain.getLetterMatrix(name, i);
            //    allMatrix.Add(newMat);
            //    if (i == 0)
            //    {
            //        rowNum[i] = newMat.Row;
            //    }
            //    else
            //    {
            //        rowNum[i] = newMat.Row + rowNum[i - 1];
            //    }
            //}
            //Console.WriteLine("read finish!");
            //Matrix totalM = new Matrix(rowNum[35], 625);
            //Matrix totalLabel = new Matrix(rowNum[35], 1);
            //int k = 0;
            //for(int t = 0; t < 36; t++)
            //{
            //    for (int i = 0; i < rowNum[t] - ((t==0)?0:rowNum[t-1]); i++)
            //    {
            //        for (int j = 0; j < 625; j++)
            //        {
            //            totalM[k, j] = allMatrix.ElementAt(t)[i, j];
            //        }
            //        totalLabel[k, 0] = -1;
            //        k++;
            //    }
            //}
            //FileStream wsfile = new FileStream("ws_lin_oneall.txt", FileMode.Create);
            //FileStream bsfile = new FileStream("bs_lin_oneall.txt", FileMode.Create);
            //for (int i = 26; i < 30; i++)
            //{
            //    Console.WriteLine("--------{0}---------", i);
            //    for(int t= ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
            //    {
            //        totalLabel[t, 0] = 1;
            //    }
            //    double b = 0;
            //    Matrix alphas = SVM.smoP(totalM, totalLabel, 1, 0.001, 100, kernelType.lin, 1, ref b);
            //    Matrix w = SVM.calcWs(alphas, totalM, totalLabel);
            //    byte[] wsdata = System.Text.Encoding.Default.GetBytes(w.Transpose().ToString());
            //    wsfile.Write(wsdata, 0, wsdata.Length);
            //    wsfile.Flush();
            //    byte[] bsdata = System.Text.Encoding.Default.GetBytes(b + "\n");
            //    bsfile.Write(bsdata, 0, bsdata.Length);
            //    bsfile.Flush();
            //    Matrix bs = new Matrix(totalM.Row, 1);
            //    bs.SetValue(b);
            //    Matrix result = Matrix.Multiply(totalM * w + bs, totalLabel);
            //    for(int t = 0; t < totalM.Row; t++)
            //    {
            //        Console.WriteLine(result[t, 0] > 0);
            //    }
            //    for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
            //    {
            //        totalLabel[t, 0] = -1;
            //    }
            //}
            //wsfile.Close();
            //bsfile.Close();
            //Console.ReadKey();
            //测试一对一线性分类下的分类情况
            //SVM.testTwoDigits(1, 0.001, kernelType.lin, 10, 6, 11);
            //生成分界参数
            //SVM.produceWsBsAs(2, 0.001, kernelType.lin, false);
            //线性分类下一对多正确率检测
            //trueRateTest();
            //测试验证码
            //Console.WriteLine(IdentifyCode.identifyLetter("pic/66.jpg",kernelType.lin, false));
            //Console.WriteLine(IdentifyCodeForKNN.identifyLetter("pic/66.jpg"));
            Console.ReadLine();
        }

        /// <summary>
        /// 线性分类一对多下的正确率情况
        /// </summary>
        public static void trueRateTest()
        {
            FileStream filews = new FileStream("ws_lin_oneall.txt", FileMode.Open);
            StreamReader readerws = new StreamReader(filews, Encoding.Default);
            FileStream filebs = new FileStream("bs_lin_oneall.txt", FileMode.Open);
            StreamReader readerbs = new StreamReader(filebs, Encoding.Default);
            Matrix ws = new Matrix(36, 625);
            Matrix bs = new Matrix(36, 1);
            double rate = 1;
            for (int i = 0; i < bs.Row; i++)
            {
                string textws = readerws.ReadLine();
                string textbs = readerbs.ReadLine();
                bs[i, 0] = double.Parse(textbs);
                string[] texts = textws.Split(' ');
                for (int j = 0; j < ws.Col; j++)
                {
                    ws[i, j] = double.Parse(texts[j]);
                }
            }
            List<Matrix> allMatrix = new List<Matrix>();
            int[] rowNum = new int[36];
            for (int i = 0; i < 36; i++)
            {
                string name = string.Format("/train/{0}", i);
                Matrix newMat = LetterMatrixForTrain.getLetterMatrix(name, i);
                allMatrix.Add(newMat);
                if (i == 0)
                {
                    rowNum[i] = newMat.Row;
                }
                else
                {
                    rowNum[i] = newMat.Row + rowNum[i - 1];
                }
            }
            Matrix totalM = new Matrix(rowNum[35], 625);
            Matrix totalLabel = new Matrix(rowNum[35], 1);
            int k = 0;
            for (int t = 0; t < 36; t++)
            {
                for (int i = 0; i < rowNum[t] - ((t == 0) ? 0 : rowNum[t - 1]); i++)
                {
                    for (int j = 0; j < 625; j++)
                    {
                        totalM[k, j] = allMatrix.ElementAt(t)[i, j];
                    }
                    totalLabel[k, 0] = -1;
                    k++;
                }
            }

            for (int i = 0; i < 36; i++)
            {
                Console.WriteLine("--------{0}---------", i);
                for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                {
                    totalLabel[t, 0] = 1;
                }

                Matrix b = new Matrix(totalM.Row, 1);
                b.SetValue(bs[i, 0]);
                Matrix result = Matrix.Multiply(totalM * ws.getRow(i).Transpose() + b, totalLabel);
                double truenum = 0;
                for (int t = 0; t < totalM.Row; t++)
                {
                    if (result[t, 0] > 0)
                    {
                        truenum++;
                    }
                }
                rate *= truenum / totalM.Row;
                Console.WriteLine(truenum / totalM.Row);
                for (int t = ((i == 0) ? 0 : rowNum[i - 1]); t < rowNum[i]; t++)
                {
                    totalLabel[t, 0] = -1;
                }
            }
            Console.WriteLine("rate:{0}", Math.Pow(rate, 4));
        }


        /// <summary>
        /// 用于生成训练集
        /// </summary>
        public static void produceTrainSet()
        {
            Cutter cutter = new Cutter(25);
            ImgGray img = new ImgGray();
            for (int i = 0; i < 1000; i++)
            {
                string name = string.Format("/{0}.jpg", i);
                Matrix mat = img.processImg(@Environment.CurrentDirectory + "/pic" + name);
                int j = 0;
                try
                {
                    foreach (Matrix matrix in cutter.Cut(mat))
                    {
                        FileStream fs = new FileStream("output/" + i + "_" + j + ".txt", FileMode.Create);
                        //获得字节数组
                        byte[] data = System.Text.Encoding.Default.GetBytes(Rotate.chooseRotate(matrix, 2, Math.PI / 18).ToString());
                        //开始写入
                        fs.Write(data, 0, data.Length);
                        //清空缓冲区、关闭流
                        fs.Flush();
                        fs.Close();
                        j++;
                    }
                }
                catch (System.Exception e)
                {
                    continue;
                }
            }
        }
        
    }
}